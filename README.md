## Minimální konfigurace pro LWJGL na macu
Projekt je připraven pro spuštění, vývojář musí pouze přidat argument pro JVM -XstartOnFirstThread

Ukázka takovéto konfigurace:

![configurace](https://bitbucket.org/HappyPy/lwjgl-mac/raw/04ec1715daa9af4a5db07c8c87fc0c918d650431/config.png)

A povolit automatický import modulů.

![configurace](https://bitbucket.org/HappyPy/lwjgl-mac/raw/ffec08534d3d1916ae0580ebede198b604b3f097/gradle.png)
